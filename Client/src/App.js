import React from 'react';

import {BrowserRouter as Router, Route} from 'react-router-dom'
import './App.css';
import Header from './components/Header/Header'
import Footer from './components/footer/Footer'
import Empleados from './components/Empleados/Empleados'
//import CreateEmpleados from './components/Empleados/CreateEmpleados'
import UpdateEmpleado from './components/Empleados/UpdateEmpleado'
import Home from './components/Home/Home'

function App() {
  return (
   
   
      <Router>
        <Header></Header>
      

        <Route  path="/" exact component={Home}/>
        <Route path="/empleados" exact component={Empleados}/>
        <Route path="/empleados/create" exact component={UpdateEmpleado}/>
        <Route path="/empleados/update/:id" exact component={UpdateEmpleado}/>
      
        <Footer></Footer>
      </Router>
    
  ); 
} 

export default App;
