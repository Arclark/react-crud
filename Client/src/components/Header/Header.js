import React, { Component } from 'react'
import {Link} from 'react-router-dom'
export default class Header extends Component {
 hrefLink="#";
    render() {
        return (

          
  <div>
  <header className="main-header">
    {/* Logo */}
    <Link to='/' className="logo">
      {/* mini logo for sidebar mini 50x50 pixels */}
      <span className="logo-mini"><b>A</b>LT</span>
      {/* logo for regular state and mobile devices */}
      <span className="logo-lg"><b>Admin</b>LTE</span>
    </Link>
    {/* Header Navbar: style can be found in header.less */}
    <nav className="navbar navbar-static-top">
      {/* Sidebar toggle button*/}
      <a href={this.hrefLink} className="sidebar-toggle" data-toggle="push-menu" role="button">
        <span className="sr-only">Toggle navigation</span>
        <span className="icon-bar" />
        <span className="icon-bar" />
        <span className="icon-bar" />
      </a>
      <div className="navbar-custom-menu">
        <ul className="nav navbar-nav">
         




          {/* User Account: style can be found in dropdown.less */}
          <li className="dropdown user user-menu">
            <a href={this.hrefLink} className="dropdown-toggle" data-toggle="dropdown">
              <img src={process.env.PUBLIC_URL+'/assets/img/user2-160x160.jpg'} className="user-image" alt="UserImage" />
              <span className="hidden-xs">Alexander Pierce</span>
            </a>
            <ul className="dropdown-menu">
              {/* User image */}
              <li className="user-header">
                <img src={process.env.PUBLIC_URL+'/assets/img/user2-160x160.jpg'} className="img-circle" alt="User" />
                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li> 
              {/* Menu Body */}
              <li className="user-body">
                <div className="row">
                  <div className="col-xs-4 text-center">
                    <a href={this.hrefLink}>Followers</a>
                  </div>
                  <div className="col-xs-4 text-center">
                    <a href={this.hrefLink}>Sales</a>
                  </div>
                  <div className="col-xs-4 text-center">
                    <a href={this.hrefLink}>Friends</a>
                  </div>
                </div>
                {/* /.row */}
              </li>
              {/* Menu Footer*/}
              <li className="user-footer">
                <div className="pull-left">
                  <a href={this.hrefLink} className="btn btn-default btn-flat">Profile</a>
                </div>
                <div className="pull-right">
                  <a href={this.hrefLink} className="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          {/* Control Sidebar Toggle Button */}
          <li>
            <a href={this.hrefLink} data-toggle="control-sidebar"><i className="fa fa-gears" /></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  {/* =============================================== */}
  {/* Left side column. contains the sidebar */}
  <aside className="main-sidebar">
    {/* sidebar: style can be found in sidebar.less */}
    <section className="sidebar">
      {/* Sidebar user panel */}
      <div className="user-panel">
        <div className="pull-left image">
          <img src={process.env.PUBLIC_URL+'/assets/img/user2-160x160.jpg'} className="img-circle" alt="UserImage" />
        </div>
        <div className="pull-left info">
          <p>Alexander Pierce</p>
          <a href={this.hrefLink}><i className="fa fa-circle text-success" /> Online</a>
        </div>
      </div>
      {/* search form */}
      <form action="#" method="get" className="sidebar-form">
        <div className="input-group">
          <input type="text" name="q" className="form-control" placeholder="Search..." />
          <span className="input-group-btn">
            <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search" />
            </button>
          </span>
        </div>
      </form>
      {/* /.search form */}
      {/* sidebar menu: : style can be found in sidebar.less */}
      <ul className="sidebar-menu" data-widget="tree">
        <li className="header">Menu de Navegacion</li>
       
       
        
      
        <li><Link to='/empleados'><i className="fa fa-book" /> <span>Empleados</span></Link></li>
       
        <li className="header">Ejemplo</li>
        
      </ul>
    </section>
    {/* /.sidebar */}
  </aside>
  {/* =============================================== */}
</div>

        )
    }
}
