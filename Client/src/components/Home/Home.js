import React, { Component } from 'react'

export default class Home extends Component {
title="Hometitle";
    render() {
        return (
            <div>
            {/* Content Wrapper. Contains page content */}
       <div className="content-wrapper">
         {/* Content Header (Page header) */}
        
         {/* Main content */}
         <section className="content">
           {/* Default box */}
           <div className="box">
             <div className="box-header with-border">
               <h3 className="box-title">{this.title}</h3>
           
             </div>
             <div className="box-body">
              <h1>home</h1>

             </div>
             {/* /.box-body */}
             <div className="box-footer">
               Pie de pagina de HOME
             </div>
             {/* /.box-footer*/}
           </div>
           {/* /.box */}
         </section>
         {/* /.content */}
       </div>
       {/* /.content-wrapper */}
       </div>

        )
    }
}
