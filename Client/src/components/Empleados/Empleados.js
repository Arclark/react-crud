import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import {GLOBAL} from '../../Config/global'
import axios from 'axios'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal)
export default class Empleados extends Component {

  state={
    empleado:[]
  }

  async componentDidMount(){ 

   this.getEmpleados() 
    
    }

    getEmpleados =async()=>{
      const resp= await axios.get(GLOBAL.url+'empleados')

    this.setState({empleado : resp.data});
    console.log(this.state.empleado)
    }

    deleteEmpleados= async (id_empleado)=>{
      MySwal.fire({
        title: '¿Desea eliminar el empleado?',
        text: "¡Se eliminara de forma permanente!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: '<li class="fa fa-reply" ></li> Cancelar',
        confirmButtonText: '<li class="fa fa-ban" ></li> Eliminar',
        focusCancel:true,
        footer:"REACT"
        
      }).then((result) => {
        if (result.value) {
  
          const resp= axios.delete(GLOBAL.url+'empleados/'+id_empleado).then(()=>{

            this.getEmpleados()
          })

                   
          if (resp) {
             MySwal.fire({
                type: 'success',
                text: 'Administración de Empleados',
                title: "Empleado eliminado con exito",
                confirmButtonText:'<i class="fa fa-check-square" ></i> Aceptar',
                footer: 'REACT',
                timer:1500
              })
          }
         
         
  
        }
      })
  
    
      
     
    }
    render() {
        return (

          <div>
          {/* Content Wrapper. Contains page content */}
     <div className="content-wrapper">
 
       {/* Main content */}
       <section className="content">
         {/* Default box */}
         <div className="box">
           <div className="box-header with-border">
             <h3 className="box-title">Empleados 
            
             </h3>
             <Link to={"/empleados/create"}>
             <button  className="btn btn-success mr-2" >NUEVO</button>
             </Link>
             
           </div>
           <div className="box-body">
            <div className="row">
              <div className="col-lg-12">
              <table className="table table-bordered table-striped table-hover">
                   <thead>
                     <tr>
                     <th> Nombre</th>
                       
                    
                       <th>
                         Telefono
                       </th>
                       <th>
                         opciones
                       </th>

                     </tr>
                   
                   </thead>
                   <tbody>
                {this.state.empleado.map(emp=>
                  
                 
                    
                    <tr key={emp.id}>
                      <td>
                        {emp.nombre}
                      </td>
                      <td>
                       {emp.telefono}
                      </td>
                      <td>
                      <div className="btn-group">
                      <Link to={'/empleados/update/'+emp.id} ><button className="btn btn-primary">editar</button></Link>
                    <button className="btn btn-danger" onClick={()=> this.deleteEmpleados(emp.id)} >Eliminar</button>
                      </div>
                      </td>
                    </tr>
                    
                  
                  )
                  }
              </tbody>
            </table>
              </div>
            </div>
            
           </div>
           {/* /.box-body */}
           <div className="box-footer">
             Footer
           </div>
           {/* /.box-footer*/}
         </div>
         {/* /.box */}
       </section>
       {/* /.content */}
     </div>
     {/* /.content-wrapper */}
     </div>

        )
    }
}
