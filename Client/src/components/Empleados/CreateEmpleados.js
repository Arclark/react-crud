import React, { Component } from 'react'
import {GLOBAL} from '../../Config/global'
import {Redirect,Link} from 'react-router-dom'
import axios from 'axios'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal)
//import swal from '@sweetalert/with-react'
export default class CreateEmpleados extends Component {

   state={
     nombre:"",
     telefono:"",
     redirect:false
   }

  
   onSubmit= async (e)=>{
     e.preventDefault();
     
     const newEmpleado={
       nombre: this.state.nombre,
       telefono: this.state.telefono
     }
     axios.post(GLOBAL.url+"empleados",newEmpleado).then(() => this.setState({ redirect: true }));
     MySwal.fire({
      type: 'success',
      text: 'Administración de Empleados',
      title: "Empleado guardado con exito",
      confirmButtonText:'<i class="fa fa-check-square" ></i> Aceptar',
      footer: 'REACT',
      timer:1500
    })
   }

   onInputChange = (e) => {
    this.setState({
        [e.target.name]: e.target.value
    })
}
    render() {
      const { redirect } = this.state;

     if (redirect) {
       return <Redirect to='/empleados'/>;
     }


        return (
            

          <div>
          {/* Content Wrapper. Contains page content */}
     <div className="content-wrapper">
 
       {/* Main content */}
       <section className="content">
         {/* Default box */}
         <div className="box">
           <div className="box-header with-border">
             <h3 className="box-title">Registrar Nuevos Empleados</h3>
            
           </div>
           <div className="box-body">


          <div className="row">
<form onSubmit={this.onSubmit}>


  <div className="col-lg-6 form-group">
              <label htmlFor=""> Nombre </label>
              <input type="text" name="nombre" id="nombre" className="form-control"
              onChange={this.onInputChange}
              value={this.state.nombre}
              />
            </div>

            <div className="col-lg-6 form-group">
              <label htmlFor=""> Telefono </label>
              <input type="text" name="telefono" id="telefono" className="form-control"
              onChange={this.onInputChange}
              value={this.state.telefono}
              />
            </div>

            <div className="col-lg-12 form-group">
              <div className="btn-group">

                <button className="btn btn-primary mr-2">Guardar</button>
                <Link to='/empleados'><button type="button"   className="btn btn-danger ml-2">Cancelar</button></Link> 
              </div>
            </div>
            </form>
          
          </div>


 
           </div>
           {/* /.box-body */}
           <div className="box-footer">
             Footer
           </div>
           {/* /.box-footer*/}
         </div>
         {/* /.box */}
       </section>
       {/* /.content */}
     </div>
     {/* /.content-wrapper */}
     </div>
        )
    }
}
