const express = require('express');
const router = express.Router();
const empleados= require('../Controllers/Empleados')
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/empleados',empleados.mostrar)
router.get('/empleados/:id',empleados.findById)
router.post('/empleados',empleados.insert)
router.put('/empleados/:id',empleados.update)
router.delete('/empleados/:id',empleados.eliminar)
router.get('/empleados/reporte',empleados.reporte)
module.exports = router;
 