'use strict'
const mysqlConnection= require('../Configuration/Database')
const PDFDocument = require('pdfkit');
const fs = require('fs');
 
// Create a document
const doc = new PDFDocument();
 

function mostrar(req, res){

    mysqlConnection.query("SELECT * FROM empleados",(err,result,fields)=>{

        if (!err) {
            res.status(200).send(result)
        }else{
            console.log(err)
        }
    })

}

function findById(req,res){
    const id=req.params.id;

    console.log(id);


    mysqlConnection.query("SELECT * FROM empleados WHERE id = ?",[id], (err,resut,fields)=>{

        if (!err) {
            res.status(200).send(resut[0])
        } else {
            console.log(err);
        }

    })
    
}


function insert(req,res){
    const {nombre,telefono}= req.body;
    mysqlConnection.query("INSERT INTO empleados(nombre, telefono) VALUES ( ? , ? )",[nombre,telefono],(err,result,fields)=>{

        if (!err) {
            res.status(200).send({message: "insertado"})
        } else {
            console.log(err)
        }
    })

}

function update(req,res){
    const {nombre,telefono}= req.body;
    const {id}= req.params;
    mysqlConnection.query("UPDATE empleados SET nombre=?,telefono=? WHERE id=?",[nombre,telefono,id],(err,result,fields)=>{

        if (!err) {
            res.status(200).send({message: "actualizado"})
        } else {
            console.log(err)
        }
    })

}

function eliminar(req,res){
  
    const {id}= req.params;
    mysqlConnection.query("DELETE FROM empleados WHERE id= ?",[id],(err,result,fields)=>{

        if (!err) {
            res.status(200).send({message: "eliminado"})
        } else {
            console.log(err)
        }
    })

}


function reporte(req,res){
 
// Pipe its output somewhere, like to a file or HTTP response
// See below for browser usage
doc.pipe(fs.createWriteStream('output.pdf'));
 
// Embed a font, set the font size, and render some text
doc.font('fonts/PalatinoBold.ttf')
   .fontSize(25)
   .text('Some text with an embedded font!', 100, 100);
 
// Add an image, constrain it to a given size, and center it vertically and horizontally
doc.image('path/to/image.png', {
   fit: [250, 300],
   align: 'center',
   valign: 'center'
});
 
// Add another page
doc.addPage()
   .fontSize(25)
   .text('Here is some vector graphics...', 100, 100);
 
// Draw a triangle
doc.save()
   .moveTo(100, 150)
   .lineTo(100, 250)
   .lineTo(200, 250)
   .fill("#FF3300");
 
// Apply some transforms and render an SVG path with the 'even-odd' fill rule
doc.scale(0.6)
   .translate(470, -380)
   .path('M 250,75 L 323,301 131,161 369,161 177,301 z')
   .fill('red', 'even-odd')
   .restore();
 
// Add some text with annotations
doc.addPage()
   .fillColor("blue")
   .text('Here is a link!', 100, 100)
   .underline(100, 100, 160, 27, {color: "#0000FF"})
   .link(100, 100, 160, 27, 'http://google.com/');
 
// Finalize PDF file
doc.end();
}
module.exports={
    mostrar,
    findById,
    insert,
    update,
    eliminar,
    reporte
}